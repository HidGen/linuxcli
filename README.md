# Linux Commands and Programming 

## Linux Commands - WorkBook

* Introduction [link](cli-workbook/0_intro.md)
* Linux CLI Environment [link](cli-workbook/1_cli.md)
* General Purpose Commands [link](cli-workbook/2_general.md)
* Handling Files & Directories [link](cli-workbook/3_files.md)
* Filter Commands [link](cli-workbook/4_filter.md)
* Shell Features [link](cli-workbook/5_shell.md)
* Customizing Environment [link](cli-workbook/6_env.md)
* File System Utils [link](cli-workbook/7_fs.md)

## Shell Scripting
* [learnshell.org](https://www.learnshell.org/)

## Programming Techniques
* GNU Tools, gcc/g++ options [click here](dev-tools/simple)
* Multifile programming [click here](dev-tools/multi)
* Makefiles [click here](dev-tools/make-tuto)
* Static Libraries [click here](dev-tools/static-libs) 
* Dynamic Libraries [click here](dev-tools/dynamic-libs) 
* Assignment-1 [click here](dev-tools/assignments/Assignment-1.md)



## Lerning resources
* [Ubuntu Tutorial](https://ubuntu.com/tutorials/command-line-for-beginners)
* [linuxjourney.com](https://linuxjourney.com/)
* [linuxsurvival.com](https://linuxsurvival.com/)
* [webminal.org](https://webminal.org/), Refer the lessons on login
* [linuxcommand.org](http://linuxcommand.org)

